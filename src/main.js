// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
Vue.prototype.obj = {}
Vue.prototype.publish = function (key, msg) {
    if (this.obj[key]) {
        this.obj[key](msg)
    }
}
Vue.prototype.Subscribe = function (key, callback) {
    this.obj[key] = callback;
}
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})
